# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = [
    url(r'^gestion/', include("shared.urls")),
    url(r'^gestion/admin/', include(admin.site.urls)),
    url(r'^gestion/selectable/', include('selectable.urls')),
    url(r'^gestion/chaining/', include('smart_selects.urls')),
]

# Serving static files in dev
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Run the init functions for each installed app
# This allow to define default instances for some models

for app in settings.INSTALLED_APPS:
    try:
        A = __import__(app)
    except ImportError:
        pass

    try:
        A.init()
    except AttributeError:
        pass

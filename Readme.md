# Installation

This application needs Python 3 and Django 1.9.

```
git clone https://git.eleves.ens.fr/ccadiou/gestioBDS.git
cd sport-ulm
pip install -r Requirements.txt
cp sportAtUlm/settings.sample.py sportAtUlm/settings.py
```

Edit the `local_settings.py` and `settings.py` files to fit your needs, and then :

```
./manage.py migrate
```

# Run

```
./manage.py runserver
```
